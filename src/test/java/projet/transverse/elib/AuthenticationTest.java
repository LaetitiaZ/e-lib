package projet.transverse.elib;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import projet.transverse.elib.domain.Address;
import projet.transverse.elib.domain.user.AccountInfo;
import projet.transverse.elib.domain.user.User;
import projet.transverse.elib.domain.user.UserRepository;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@AutoConfigureMockMvc
public class AuthenticationTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationProvider provider;

    @Autowired
    MockMvc mockMvc;

    final String VALID_LOGIN = "user1@email.com";
    final String VALID_PASSWORD = "password";
    final String INVALID_PASSWORD = "oh-so-much-wrong";
    final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    User user = new User();

    @BeforeEach
    void createUserInDB() {
        String encodedPassword = passwordEncoder.encode(VALID_PASSWORD);

        AccountInfo accountInfo = new AccountInfo(VALID_LOGIN, encodedPassword);
        user = new User("first", "second", new Address("", "", "", ""), accountInfo);
        userRepository.add(user);
    }

    @AfterEach
    void deleteUserInDB() {
        userRepository.delete(user);
    }

    @Test
    void registered_user_can_login_with_valid_login_and_valid_password() {
        AccountInfo loginInfo = new AccountInfo(VALID_LOGIN, VALID_PASSWORD);

        Authentication authenticate = provider.authenticate(loginInfo);

        assertThat(authenticate).isNotNull();
    }

    @Test
    void should_not_login_with_wrong_password() {
        AccountInfo loginInfo = new AccountInfo(VALID_LOGIN, INVALID_PASSWORD);

        Authentication authenticate = provider.authenticate(loginInfo);

        assertThat(authenticate).isNull();
    }

    @Test
    void should_login_calling_post_login_http_basic() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/home").with(httpBasic(VALID_LOGIN, VALID_PASSWORD)))
                .andExpect(status().isOk());
    }

}
