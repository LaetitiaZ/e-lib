package projet.transverse.elib;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import projet.transverse.elib.application.book.BookService;
import projet.transverse.elib.domain.book.Book;
import projet.transverse.elib.domain.book.BookRepository;
import projet.transverse.elib.domain.book.Comment;
import projet.transverse.elib.domain.book.Genre;

import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class BookTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    BookService bookService;

    @Autowired
    BookRepository bookRepository;

    @Test
    void should_add_to_db_when_call_to_add_book_method() {
        Book book = new Book("isbn1", "title", "author", Genre.Mystery, new Date(), "it is a book for test");

        bookService.addBook(book);

        Optional<Book> isbn1 = bookRepository.findById("isbn1");
        assertThat(isbn1).isPresent();
    }

    @Test
    void add_tag_should_modify_book_object_in_db() {
        Book book = new Book("isbn1", "title", "author", Genre.Mystery, new Date(), "it is a book for test");
        bookService.addBook(book);

        String firstTag = "first tag for test";
        bookService.addTag("isbn1", firstTag);
        Book isbn1 = bookRepository.findById("isbn1").get();

        assertThat(isbn1.getTags()).contains(firstTag);
    }

    @Test
    void add_comment_should_modify_book_object_in_db() {
        Book book = new Book("isbn1", "title", "author", Genre.Mystery, new Date(), "it is a book for test");
        bookService.addBook(book);

        bookService.addComment("isbn1", new Comment("id","ad",1));
        Book isbn1 = bookRepository.findById("isbn1").get();

        assertThat(isbn1.getComments()).isNotNull();
    }


}
