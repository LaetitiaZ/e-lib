package projet.transverse.elib;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import projet.transverse.elib.domain.book.Book;
import projet.transverse.elib.domain.book.BookRepository;
import projet.transverse.elib.domain.book.Genre;

import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@Transactional
public class AdminBookControllerIT {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    BookRepository bookRepository;

    @Test
    @WithMockUser(roles = {"ADMIN"})
    void call_to_controller_to_with_book_in_body_should_add_in_db() throws Exception {
        Book book = new Book("isbn1", "title", "author", Genre.Mystery, new Date(), "it is a book for test");

        mockMvc.perform(post("/admin/books/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isOk());

        Optional<Book> optionalBook = bookRepository.findById("isbn1");
        assertThat(optionalBook).isPresent();
    }
}
