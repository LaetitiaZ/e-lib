package projet.transverse.elib.exposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import projet.transverse.elib.application.book.BookService;
import projet.transverse.elib.application.user.UserService;
import projet.transverse.elib.domain.book.Book;
import projet.transverse.elib.domain.book.Comment;
import projet.transverse.elib.domain.library.Loan;
import projet.transverse.elib.domain.user.AccountInfo;
import projet.transverse.elib.domain.user.User;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    BookService bookService;

    @Autowired
    UserService userService;

    @GetMapping("/display")
    public List<Book> display() {
        return bookService.getBooks();
    }

    @PostMapping("/{isbn}/add-comment")
    public void addComment(@PathVariable String isbn, @RequestBody Comment comment) {
        bookService.addComment(isbn, comment);
    }

    @GetMapping("/{isbn}")
    public Book getBook(@PathVariable String isbn) {
        return bookService.getBook(isbn);
    }

    /** Loan duration is 3 months **/
    @PostMapping("/{isbn}/loan")
    public void createLoan(@PathVariable String isbn, Authentication authentication) {
        AccountInfo accountInfo = (AccountInfo) authentication.getPrincipal();
        String mail = accountInfo.getMail();
        User user = userService.getUser(mail);

        userService.addLoan(isbn, user);
    }

    @GetMapping("/{isbn}/read-permission")
    public boolean isAbleToRead(@PathVariable String isbn, Authentication authentication) {
        AccountInfo accountInfo = (AccountInfo) authentication.getPrincipal();
        String mail = accountInfo.getMail();
        User user = userService.getUser(mail);

        Loan loan = userService.getLoan(isbn, user);
        return loan.isValid();
    }

    @GetMapping("/{isbn}/location")
    public String bookLocation(@PathVariable String isbn) {
        Book book = bookService.getBook(isbn);
        return book.getLocation();
    }
}
