package projet.transverse.elib.exposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import projet.transverse.elib.application.user.UserService;
import projet.transverse.elib.domain.book.Book;
import projet.transverse.elib.domain.book.BookRepository;
import projet.transverse.elib.domain.user.User;

import javax.servlet.http.HttpServletResponse;

@RestController
public class HomeController {

    @Autowired
    UserService userService;

    @Autowired
    BookRepository bookRepository;

    @GetMapping("/home")
    String getHome() {
        return "home";
    }

    @PostMapping("/home")
    String getHomeFromLogin(Authentication authentication) {
        return "Hello you came from login";
    }

    @PostMapping("/signup")
    void signup(@RequestBody User user) {
        if(!userService.getOptionalUser(user.getAccountInfo().getMail()).isPresent()) {
            userService.createAccount(user);
        }
    }

    @PostMapping("/signin")
    void login() {
    }

}

