package projet.transverse.elib.exposition.admin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/library")
public class LibraryInformationController {

    @Value("library.name")
    private String libraryName;

    @GetMapping("/name")
    public String getLibraryName() {
        return libraryName;
    }

}
