package projet.transverse.elib.exposition.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import projet.transverse.elib.application.book.BookService;
import projet.transverse.elib.domain.book.Book;
import projet.transverse.elib.domain.book.Comment;

import java.util.List;

@RestController
@RequestMapping("/admin/books")
public class AdminBookController {

    @Autowired
    BookService bookService;

    @GetMapping("/display")
    public List<Book> display() {
        return bookService.getBooks();
    }

    @PostMapping("/add")
    public void addBook(@RequestBody Book book) {
        bookService.addBook(book);
    }

    @PostMapping("/{isbn}/add-tag")
    public void addTag(@PathVariable String isbn, @RequestBody String tag) {
        bookService.addTag(isbn, tag);
    }

    @PostMapping("/{isbn}/add-comment")
    public void addComment(@PathVariable String isbn, @RequestBody Comment comment) {
        bookService.addComment(isbn, comment);
    }

    @GetMapping("/{isbn}")
    public Book getBook(@PathVariable String isbn) {
        return bookService.getBook(isbn);
    }

    @PostMapping("/{isbn}/delete-tag")
    public void deleteTag(@PathVariable String isbn, @RequestBody String tag) {
        bookService.deleteTag(isbn, tag);
    }

    @DeleteMapping("/{isbn}/delete")
    public void deleteBook(@PathVariable String isbn) {
        bookService.deleteBook(isbn);
    }
}
