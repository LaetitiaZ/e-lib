package projet.transverse.elib.domain.user;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Embeddable
public class AccountInfo implements Authentication {


    private String mail;
    private String password;

    @ElementCollection
    private List<String> authorities = new ArrayList<>();

    public AccountInfo(String password, String mail, List<String> authorities) {
        this.password = password;
        this.mail = mail;
        this.authorities = authorities;
    }

    public AccountInfo(String mail, String password) {
        this.password = password;
        this.mail = mail;
    }

    public AccountInfo() {

    }

    @Override
    public String getName() {
        return mail;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String authority : this.authorities) {
            authorities.add(new SimpleGrantedAuthority(authority));
        }
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return password;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return this;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setAuthorities(List<GrantedAuthority> authorities) {
        this.authorities.clear();
        authorities.forEach(authority -> this.authorities.add(authority.getAuthority()));
    }
}
