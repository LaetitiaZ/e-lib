package projet.transverse.elib.domain.user;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    void add(User user);
    void update(User user);
    void delete(User user);
    Optional<User> findById(String id);
    List<User> findAll();
    Optional<User> findByMail(String mail);
}
