package projet.transverse.elib.domain.library;

import javax.persistence.*;
import java.time.Duration;
import java.util.Date;

@Entity
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private Date start;
    private String receiverID;
    private String bookID;
    private int durationInDays;

    public boolean isValid() {
        //TODO should check validity of start and duration values (not null)
        Duration duration = Duration.parse("PT"+durationInDays+"M");
        return start.toInstant().plus(duration).isAfter(new Date(System.currentTimeMillis()).toInstant());
    }

    public Loan(Date start, String receiverID, String bookID, int duration) {
        this.start = start;
        this.receiverID = receiverID;
        this.bookID = bookID;
        this.durationInDays = duration;
    }

    public Loan() {
    }

//    public String getId() {
//        return id;
//    }

    public Date getStart() {
        return start;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public String getBookID() {
        return bookID;
    }

    public int getDurationInDays() {
        return durationInDays;
    }

}
