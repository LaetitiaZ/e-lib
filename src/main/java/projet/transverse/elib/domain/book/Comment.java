package projet.transverse.elib.domain.book;

import javax.persistence.Embeddable;

@Embeddable
public class Comment {

    private String authorID;
    private String content;
    private int rate;

    public Comment(String authorID, String content, int rate) {
        this.authorID = authorID;
        this.content = content;
        this.rate = rate;
    }

    public Comment() {

    }

    public String getAuthorID() {
        return authorID;
    }

    public String getContent() {
        return content;
    }

    public int getRate() {
        return rate;
    }

    public void setAuthorID(String authorID) {
        this.authorID = authorID;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
