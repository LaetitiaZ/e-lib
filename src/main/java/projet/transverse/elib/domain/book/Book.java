package projet.transverse.elib.domain.book;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Book {

    @Id
    private String isbn;

    private String title;
    private String author;
    private Genre genre;
    private Date date;
    private String summary;
    private int rate;

    @ElementCollection
    private List<Comment> comments = new ArrayList<>();
    @ElementCollection
    private List<String> tags = new ArrayList<>();

    private String location;

    public List<Comment> getComments() {
        return comments;
    }

    public List<String> getTags() {
        return tags;
    }

    public Book(String isbn, String title, String author, Genre genre, Date date, String summary) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.date = date;
        this.summary = summary;
    }

    public Book() {
    }

    public Book(String isbn) {
        this.isbn = isbn;
    }

    public Book(String isbn, String title, String author, Genre genre, Date date, String summary, int rate, List<Comment> comments, List<String> tags, String location) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.date = date;
        this.summary = summary;
        this.rate = rate;
        this.comments = comments;
        this.tags = tags;
        this.location = location;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public Genre getGenre() {
        return genre;
    }

    public Date getDate() {
        return date;
    }

    public String getSummary() {
        return summary;
    }

    public int getRate() {
        return rate;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getLocation() {
        return location;
    }

}
