package projet.transverse.elib.domain.book;

import java.util.List;
import java.util.Optional;

public interface BookRepository {

    void add(Book book);
    void delete(Book book);
    Optional<Book> findById(String id);
    void deleteById(String id);
    List<Book> findAll();
    void update(Book book);

}
