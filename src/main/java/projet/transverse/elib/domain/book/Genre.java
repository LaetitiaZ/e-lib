package projet.transverse.elib.domain.book;

public enum Genre {
    Adventure,
    Contemporary,
    Fantasy,
    Horror,
    Mystery,
    Romance,
    Thriller,
    Novel,
    Educational
}
