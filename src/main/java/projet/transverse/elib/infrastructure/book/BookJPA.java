package projet.transverse.elib.infrastructure.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import projet.transverse.elib.domain.book.Book;

@Repository
public interface BookJPA extends JpaRepository<Book, String> {
}
