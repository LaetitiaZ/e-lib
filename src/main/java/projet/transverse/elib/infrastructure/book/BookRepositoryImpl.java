package projet.transverse.elib.infrastructure.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import projet.transverse.elib.domain.book.Book;
import projet.transverse.elib.domain.book.BookRepository;

import java.util.List;
import java.util.Optional;

@Repository
public class BookRepositoryImpl implements BookRepository {

    private final BookJPA bookJPA;

    @Autowired
    public BookRepositoryImpl(BookJPA bookJPA) {
        this.bookJPA = bookJPA;
    }

    @Override
    public void add(Book book) {
        bookJPA.save(book);
    }

    public void update(Book book) {
        bookJPA.save(book);
    }

    @Override
    public void delete(Book book) {
        bookJPA.delete(book);
    }

    @Override
    public Optional<Book> findById(String id) {
        return bookJPA.findById(id);
    }

    @Override
    public void deleteById(String id) {
        bookJPA.deleteById(id);
    }

    @Override
    public List<Book> findAll() {
        return bookJPA.findAll();
    }

}
