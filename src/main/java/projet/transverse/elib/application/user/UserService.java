package projet.transverse.elib.application.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import projet.transverse.elib.domain.library.Loan;
import projet.transverse.elib.domain.user.User;
import projet.transverse.elib.domain.user.UserRepository;
import projet.transverse.elib.security.Utils;

import java.util.Date;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void createAccount(User user) {
        String raw = user.getAccountInfo().getPassword();
        BCryptPasswordEncoder bCryptPasswordEncoder = Utils.encoder;
        String encodedPassword = bCryptPasswordEncoder.encode(raw);
        user.getAccountInfo().setPassword(encodedPassword);
        userRepository.add(user);
    }

    public void addLoan(String isbn, User user) {
        Date todayDate = new Date(System.currentTimeMillis());
        Loan loan = new Loan(todayDate, user.getAccountInfo().getMail(), isbn, 30);
        user.addLoan(loan);
        userRepository.update(user);
    }

    public User getUser(String mail) {
        return userRepository.findByMail(mail)
                .orElseThrow(() -> new IllegalArgumentException("No user found for mail "+ mail));
    }

    public Optional<User> getOptionalUser(String mail) {
        return userRepository.findByMail(mail);
    }

    public Loan getLoan(String isbn, User user) {
         return user.getLoans()
                 .stream()
                 .filter(loan -> loan.getBookID().equals(isbn))
                 .findFirst()
                 .orElseThrow(() -> new IllegalArgumentException("No book found for ISBN "+isbn));
    }


}
