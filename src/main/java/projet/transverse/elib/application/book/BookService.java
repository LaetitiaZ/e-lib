package projet.transverse.elib.application.book;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import projet.transverse.elib.domain.book.Book;
import projet.transverse.elib.domain.book.BookRepository;
import projet.transverse.elib.domain.book.Comment;

import java.util.List;

@Service
@Transactional
public class BookService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookService.class);
    @Autowired
    BookRepository bookRepository;

    public void addBook(Book book) {
        bookRepository.add(book);
    }

    public void addComment(String isbn, Comment comment) {
        Book book = bookRepository.findById(isbn)
                .orElseThrow(() -> new IllegalArgumentException("No book found for id " + isbn));

        book.getComments().add(comment);

        bookRepository.update(book);
    }

    public void addTag(String isbn, String tag) {
        Book book = getBook(isbn);

        book.getTags().add(tag);

        bookRepository.update(book);
    }

    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    public Book getBook(String isbn) {
        return bookRepository.findById(isbn)
                .orElseThrow(() -> new IllegalArgumentException("No book found for id " + isbn));
    }

    public void deleteBook(String isbn) {
        bookRepository.deleteById(isbn);
    }

    public void deleteTag(String isbn, String tag) {
        Book book = getBook(isbn);
        boolean hasRemoved = book.getTags().remove(tag);

        if(!hasRemoved) {
            LOGGER.warn("Tag {} from book with ISBN {} couldn't be deleted", isbn, tag);
        }
    }


}
